/*
Copyright (c) 2009 by Juliusz Chroboczek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "threadpool.h"

#include "debug/debug.h"

#include <SDL_atomic.h>
#include <SDL_thread.h>
#include <SDL_mutex.h>

#include <stdlib.h>
#include <errno.h>
#include <time.h>

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#define EXTRA_LEAN
#include <Windows.h>
#else
#include <pthread.h>
#endif

typedef struct threadpool_item {
    threadpool_func_t *func;
    void *closure;
    struct threadpool_item *next;
} threadpool_item_t;

typedef struct threadpool_queue {
    threadpool_item_t *first;
    threadpool_item_t *last;
} threadpool_queue_t;

struct threadpool {
    int maxthreads, threads, idle;
    threadpool_queue_t scheduled, scheduled_back;
    /* Set when we request that all threads die. */
    int dying;
    /* If this is false, we are guaranteed that scheduled_back is empty. */
    SDL_atomic_t have_scheduled_back;
    /* Protects everything except the atomics above. */
    SDL_mutex* lock;
    /* Signaled whenever a new continuation is enqueued or dying is set. */
    SDL_cond* cond;
    /* Signaled whenever a thread dies. */
    SDL_cond* die_cond;
    threadpool_func_t *wakeup;
    void *wakeup_closure;
};

threadpool_t *
threadpool_create(int maxthreads,
                  threadpool_func_t *wakeup, void *wakeup_closure)
{
    threadpool_t *tp;
    tp = calloc(1, sizeof(threadpool_t));
    if(tp == NULL)
        return NULL;

    tp->maxthreads = maxthreads;
    tp->wakeup = wakeup;
    tp->wakeup_closure = wakeup_closure;
    
	SDL_AtomicSet(&tp->have_scheduled_back, 0);
	tp->lock = SDL_CreateMutex();
	tp->cond = SDL_CreateCond();
	tp->die_cond = SDL_CreateCond();

    return tp;
}

int
threadpool_die(threadpool_t *threadpool, int canblock)
{
    int done;

	SDL_LockMutex(threadpool->lock);

    threadpool->dying = 1;
	SDL_CondBroadcast(threadpool->cond);

    while(threadpool->threads > 0) {
        if(threadpool->scheduled_back.first || !canblock)
            break;
		SDL_CondWait(threadpool->die_cond, threadpool->lock);
    }

    done = threadpool->threads == 0;

	SDL_UnlockMutex(threadpool->lock);
    return done;
}

int
threadpool_destroy(threadpool_t *threadpool)
{
    int dead;

	SDL_LockMutex(threadpool->lock);
    dead =
        threadpool->threads == 0 &&
        threadpool->scheduled.first == NULL &&
        threadpool->scheduled_back.first == NULL;
	SDL_UnlockMutex(threadpool->lock);

    if(!dead)
        return -1;

	SDL_DestroyCond(threadpool->cond);
	SDL_DestroyCond(threadpool->die_cond);
	SDL_DestroyMutex(threadpool->lock);
    free(threadpool);
    return 1;
}

static threadpool_item_t *
threadpool_dequeue(threadpool_queue_t *queue)
{
    threadpool_item_t *item;

    if(queue->first == NULL)
        return NULL;

    item = queue->first;
    queue->first = item->next;
    if(item->next == NULL)
        queue->last = NULL;
    return item;
}

static int
thread_main(void *pool)
{
    threadpool_t *threadpool = pool;
    threadpool_item_t *item;
    threadpool_func_t *func;
    void *closure;

again:
	SDL_LockMutex(threadpool->lock);

    if(threadpool->scheduled.first == NULL) {
        if(threadpool->dying)
            goto die;

        /* Beware when benchmarking.  Under Linux with NPTL, idle threads
           are slightly counter-productive in some benchmarks, but
           extremely productive in others. */

        /* This constant may need to be tweaked. */
        if(threadpool->idle >= 2)
            goto die;
		
        threadpool->idle++;
		SDL_CondWaitTimeout(threadpool->cond, threadpool->lock, 1000);
        threadpool->idle--;
        if(threadpool->scheduled.first == NULL)
            goto die;
    }

    item = threadpool_dequeue(&threadpool->scheduled);
	SDL_UnlockMutex(threadpool->lock);

    func = item->func;
    closure = item->closure;
    free(item);

    func(closure);
    goto again;

 die:
    threadpool->threads--;
	SDL_CondBroadcast(threadpool->die_cond);
	SDL_UnlockMutex(threadpool->lock);
    return 0;
}

/* This is called with the pool locked. */
static int
threadpool_new_thread(threadpool_t *threadpool)
{
	SDL_Thread* thread = NULL;
    ASSERT(threadpool->threads < threadpool->maxthreads);

	thread = SDL_CreateThread(thread_main, "worker", (void*)threadpool);
    if(thread == NULL) {
        return -1;
    }
	SDL_DetachThread(thread);
    threadpool->threads++;
    return 1;
}

/* There's a reason for the inconvenient interface: we want to perform the
   allocation outside of the critical region, and only take the lock when
   inserting the new cell. */

static threadpool_item_t *
threadpool_item_alloc(threadpool_func_t *func, void *closure)
{
    threadpool_item_t *item;

    item = malloc(sizeof(threadpool_item_t));
    if(item == NULL)
        return NULL;

    item->func = func;
    item->closure = closure;
    item->next = NULL;

    return item;
}

static void
threadpool_enqueue(threadpool_queue_t *queue, threadpool_item_t *item)
{
    item->next = NULL;
    if(queue->last)
        queue->last->next = item;
    else
        queue->first = item;
    queue->last = item;
}

int
threadpool_schedule(threadpool_t *threadpool,
                    threadpool_func_t *func, void *closure)
{
    threadpool_item_t *item;
    int rc = 0;
    int dosignal = 1;

    item = threadpool_item_alloc(func, closure);
    if(item == NULL)
        return -1;

	SDL_LockMutex(threadpool->lock);
    threadpool_enqueue(&threadpool->scheduled, item);
    if(threadpool->idle == 0) {
        dosignal = 0;
        if(threadpool->threads < threadpool->maxthreads) {
            rc = threadpool_new_thread(threadpool);
            if(rc < 0 && threadpool->threads > 0)
                rc = 0;             /* we'll recover */
        }
    }
	if (dosignal)
		SDL_CondSignal(threadpool->cond);
	SDL_UnlockMutex(threadpool->lock);

    return rc;
}

int
threadpool_schedule_back(threadpool_t *threadpool,
                         threadpool_func_t *func, void *closure)
{
    threadpool_item_t *item;
    int wake = 1;

    item = threadpool_item_alloc(func, closure);
    if(item == NULL)
        return -1;

	SDL_LockMutex(threadpool->lock);
    if(SDL_AtomicGet(&threadpool->have_scheduled_back))
        wake = 0;
    /* Order is important. */
	SDL_AtomicSet(&threadpool->have_scheduled_back, 1);
    threadpool_enqueue(&threadpool->scheduled_back, item);
	SDL_UnlockMutex(threadpool->lock);

    if(wake && threadpool->wakeup)
        threadpool->wakeup(threadpool->wakeup_closure);

    return 0;
}

void
threadpool_run_callbacks(threadpool_t *threadpool)
{
    threadpool_item_t *items;

    if(!SDL_AtomicGet(&threadpool->have_scheduled_back))
        return;

	SDL_LockMutex(threadpool->lock);
    items = threadpool->scheduled_back.first;
    /* Order is important. */
    threadpool->scheduled_back.first = NULL;
    threadpool->scheduled_back.last = NULL;
	SDL_AtomicSet(&threadpool->have_scheduled_back, 0);
	SDL_UnlockMutex(threadpool->lock);

    while(items) {
        threadpool_item_t *first;
        threadpool_func_t *func;
        void *closure;
        first = items;
        items = items->next;
        func = first->func;
        closure = first->closure;
        free(first);
        func(closure);
    }
}

unsigned threadpool_hardware_concurrency()
{
#if defined(PTW32_VERSION) || defined(__hpux)
	return pthread_num_processors_np();
#elif defined(__APPLE__) || defined(__FreeBSD__)
	int count;
	size_t size = sizeof(count);
	return sysctlbyname("hw.ncpu", &count, &size, NULL, 0) ? 0 : count;
#elif defined(BOOST_HAS_UNISTD_H) && defined(_SC_NPROCESSORS_ONLN)
	int const count = sysconf(_SC_NPROCESSORS_ONLN);
	return (count > 0) ? count : 0;
#elif defined(_GNU_SOURCE)
	return get_nprocs();
#elif _WIN32
	SYSTEM_INFO info = { { 0 } };
	GetSystemInfo(&info);
	return info.dwNumberOfProcessors;
#else
	return 0;
#endif
}
