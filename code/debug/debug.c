#include "debug.h"

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#define EXTRA_LEAN
#include <Windows.h>
#else
#include <stdio.h>
#endif

void TRACE_impl(const char * text)
{
#if defined(_WIN32)
	OutputDebugStringA(text);
#else
	puts(text);
#endif
}
