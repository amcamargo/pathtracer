#pragma once

void TRACE_impl(const char *text);

#define TRACE TRACE_impl

#if defined(_DEBUG)
#if defined(_WIN32)
#include <crtdbg.h>
#define ASSERT _ASSERTE
#else
#include <assert.h>
#define ASSERT assert
#endif

#else
#define ASSERT(x) ((void)0)
#endif