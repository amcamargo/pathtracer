#include "surface.h"

#include "debug/debug.h"

#include <memory.h>

surface *surface_create(surface *out, uint32_t w, uint32_t h, uint8_t data_sz)
{
	out->width = w;
	out->height = h;
	out->data_size = data_sz;

	const uint32_t buff_sz = data_sz * w * h;
	out->buffer = malloc(buff_sz);

	memset(out->buffer, 0, buff_sz);

	return out;
}

surface *surface_copy_from32_to8(surface *out, const surface *in)
{
	ASSERT(in->data_size == sizeof(color32) && out->data_size == sizeof(color8));
	ASSERT(in->width == out->width);
	ASSERT(in->height == out->height);

	const uint64_t total_size = in->width * in->height;
	const color32* c32 = (const color32*)in->buffer;
	color8* c8 = (color8*)out->buffer;
	
	for (uint64_t i = 0; i < total_size; ++i)
	{
		c8[i] = color32_to_color8(&c32[i]);
	}

	return out;
}

void surface_free(surface *in)
{
	free(in->buffer);
	memset(in, 0, sizeof(surface));
}
