#pragma once

#include "math/ray.h"

typedef struct camera
{
	/*
	 * world-space position
	 */
	vector4 pos;
	
	vector4 w;
	vector4 u;
	vector4 v;

	float_t fovx;
	float_t fovy;
	float_t halfw;
	float_t halfh;
} camera;

#ifdef __cplusplus
extern "C" {
#endif

	/*
	 * FOV in radians
	 */
	camera *camera_create(camera *out, const vector4 *eye, const vector4 *center, const vector4 *up, float_t width, float_t height, float_t fov);

	ray *camera_get_ray(ray *out, const camera *lhs, float_t i, float_t j);

#ifdef __cplusplus
}
#endif