#include "camera.h"

camera *camera_create(camera *out, const vector4 *eye, const vector4 *center, const vector4 *up, float_t width, float_t height, float_t fov)
{
	out->pos = *eye;

	vector4_normalize(&out->w, vector4_sub(&out->w, eye, center));
	vector4_normalize(&out->u, vector4_cross3(&out->u, up, &out->w));
	vector4_cross3(&out->v, &out->w, &out->u);

	out->fovy = tanf(fov * .5f);
	out->fovx = out->fovy * (width / height);

	out->halfw = width * .5f;
	out->halfh = height * .5f;

	return out;
}

ray *camera_get_ray(ray *out, const camera *lhs, float_t i, float_t j)
{
	// shoot through center of pixel
	//const float_t a = lhs->fovx * ((i + .5f - lhs->halfw) / lhs->halfw);
	//const float_t b = lhs->fovy * ((j + .5f - lhs->halfh) / lhs->halfh);

	const float_t a = lhs->fovx * ((i - lhs->halfw) / lhs->halfw);
	const float_t b = lhs->fovy * ((j - lhs->halfh) / lhs->halfh);

	out->origin = lhs->pos;
	
	vector4 au;
	vector4 bv;

	vector4_normalize(&out->direction, vector4_sub(&out->direction, vector4_add(&out->direction, vector4_scale(&au, &lhs->u, a), vector4_scale(&bv, &lhs->v, b)), &lhs->w));

	return out;
}
