#pragma once

#include "math/color.h"

typedef struct surface
{
	uint8_t *buffer;
	uint32_t width;
	uint32_t height;
	uint8_t data_size;
} surface;

#ifdef __cplusplus
extern "C" {
#endif

	surface *surface_create(surface *out, uint32_t w, uint32_t h, uint8_t data_sz);

	surface *surface_copy_from32_to8(surface *out, const surface *in);

	void surface_free(surface *in);

#ifdef __cplusplus
}
#endif