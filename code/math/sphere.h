#pragma once

#include "vector4.h"
#include "color.h"
#include "ray.h"

typedef enum object_type
{
	SPHERE,
	PLANE,
} object_type;

typedef struct sphere
{
	/*
	 * Center in world space
	 */
	vector4 center;
	float_t radius;
	object_type type;
} sphere;

#ifdef __cplusplus
extern "C" {
#endif

	float_t sphere_intersect_ray(const sphere *lhs, const ray *rhs);

	/*
	 * if type == PLANE, Interprets a sphere as a plane described by:
	 * - a normal (center)
	 * - and an offset along that axis (radius)
	 */
	float_t plane_intersect_ray(const sphere *lhs, const ray *rhs);

	vector4 *sphere_get_normal(vector4 *out, const sphere *in, const vector4 *ref_point);

#ifdef __cplusplus
}
#endif
