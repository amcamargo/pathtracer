#pragma once

#include "vector4.h"

typedef struct ray
{
	vector4 origin;
	vector4 direction;
} ray;

#ifdef __cplusplus
extern "C" {
#endif

	vector4* ray_get_position(vector4 *out, const ray *r, float_t t);
	
#ifdef __cplusplus
}
#endif