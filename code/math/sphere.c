#include "sphere.h"

#include "float.h"

float_t sphere_intersect_ray(const sphere *lhs, const ray *rhs)
{
	vector4 orig_center;
	vector4_sub(&orig_center, &rhs->origin, &lhs->center);

	const float_t a = vector4_dot(&rhs->direction, &rhs->direction);
	const float_t b = 2.0f * vector4_dot(&rhs->direction, &orig_center);
	const float_t c = vector4_dot(&orig_center, &orig_center) - lhs->radius * lhs->radius;

	float_t t0, t1;
	if (quadratic(a, b, c, &t0, &t1)) {
		if (t0 > .0f && t1 > .0f) {
			return (t0 < t1) ? t0 : t1;
		}
	}
	
	return .0f;
}

float_t plane_intersect_ray(const sphere *lhs, const ray *rhs)
{
	vector4 normal;
	vector4_scale(&normal, &lhs->center, -1.0f);
	const float_t t = (lhs->radius - vector4_dot(&normal, &rhs->origin)) / vector4_dot(&normal, &rhs->direction);

	if (t > FLT_EPSILON)
		return t;

	return .0f;
}

vector4 *sphere_get_normal(vector4 *out, const sphere *in, const vector4 *ref_point)
{
	if (in->type == PLANE) {
		*out = in->center;
	}
	else {
		vector4_sub(out, ref_point, &in->center);
		vector4_normalize(out, out);
	}

	return out;
}
