#include "vector4.h"

#include "debug/debug.h"

const vector4 VECTOR4_ZERO = { .0f, .0f, .0f, .0f };
const vector4 VECTOR4_UP = { .0f, 1.0f, .0f, .0f };
const vector4 VECTOR4_LEFT = { -1.0f, .0f, .0f, .0f };
const vector4 VECTOR4_RIGHT = { 1.0f, .0f, .0f, .0f };
const vector4 VECTOR4_FORWARD = { .0f, .0f, 1.0f, .0f };

vector4 *vector4_add(vector4 *out, const vector4 *lhs, const vector4 *rhs)
{
	out->x = lhs->x + rhs->x;
	out->y = lhs->y + rhs->y;
	out->z = lhs->z + rhs->z;
	out->w = lhs->w + rhs->w;

	return out;
}

vector4 *vector4_sub(vector4 *out, const vector4 *lhs, const vector4 *rhs)
{
	out->x = lhs->x - rhs->x;
	out->y = lhs->y - rhs->y;
	out->z = lhs->z - rhs->z;
	out->w = lhs->w - rhs->w;

	return out;
}

float_t vector4_dot(const vector4 *lhs, const vector4 *rhs)
{
	return (lhs->x * rhs->x) + (lhs->y * rhs->y) + (lhs->z * rhs->z) + (lhs->w * rhs->w);
}

vector4 *vector4_cross3(vector4 *out, const vector4 *lhs, const vector4 *rhs)
{
	ASSERT(out != lhs && out != rhs);

	out->x = (lhs->y  * rhs->z) - (rhs->y * lhs->z);
	out->y = (-lhs->x * rhs->z) + (rhs->x * lhs->z);
	out->z = (lhs->x  * rhs->y) - (rhs->x * lhs->y);
	out->w = .0f;

	return out;
}

vector4 *vector4_scale(vector4 *out, const vector4 *lhs, float_t s)
{
	out->x = lhs->x * s;
	out->y = lhs->y * s;
	out->z = lhs->z * s;
	out->w = lhs->w * s;

	return out;
}

vector4 *vector4_normalize(vector4 *out, const vector4 *in)
{
	const float_t l = fast_invsqrtf(vector4_dot(in, in));
	vector4_scale(out, in, l);
	return out;
}

vector4 *vector4_random_dir_hemisphere(vector4 *out, const vector4 *dir, uint16_t *rseed)
{
	*out = VECTOR4_ZERO;

	vector4 w = *dir;
	vector4 u; vector4_normalize(&u, vector4_cross3(&u, fast_absf(w.x) > .1f ? &VECTOR4_UP : &VECTOR4_RIGHT, &w));
	vector4 v; vector4_cross3(&v, &w, &u);

	const float_t a = 2 * (float_t)M_PI * frand48(rseed);
	const float_t d = frand48(rseed);
	const float_t d2s = fast_sqrtf(d);

	vector4_add(out, out, vector4_scale(&u, &u, cosf(a) * d2s));
	vector4_add(out, out, vector4_scale(&v, &v, sinf(a) * d2s));
	vector4_add(out, out, vector4_scale(&w, &w, fast_sqrtf(1.f - d)));

	vector4_normalize(out, out);

	return out;
}

vector4 *vector4_random_dir_sphere(vector4 *out, uint16_t *rseed)
{
	out->x = (2.f * frand48(rseed)) - frand48(rseed);
	out->y = (2.f * frand48(rseed)) - frand48(rseed);
	out->z = (2.f * frand48(rseed)) - frand48(rseed);
	out->w = .0f;

	vector4_normalize(out, out);

	return out;
}
