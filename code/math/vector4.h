#pragma once

#include "fastmath.h"

struct sphere;

typedef struct vector4
{
	union {
		float_t values[4];
		struct {
			float_t x;
			float_t y;
			float_t z;
			float_t w;
		};
	};
} vector4;

#ifdef __cplusplus
extern "C" {
#endif

	extern const vector4 VECTOR4_ZERO;
	extern const vector4 VECTOR4_UP;
	extern const vector4 VECTOR4_LEFT;
	extern const vector4 VECTOR4_RIGHT;
	extern const vector4 VECTOR4_FORWARD;

	vector4 *vector4_add(vector4 *out, const vector4 *lhs, const vector4 *rhs);

	vector4 *vector4_sub(vector4 *out, const vector4 *lhs, const vector4 *rhs);

	float_t vector4_dot(const vector4 *lhs, const vector4 *rhs);

	vector4 *vector4_cross3(vector4 *out, const vector4 *lhs, const vector4 *rhs);

	vector4 *vector4_scale(vector4 *out, const vector4 *lhs, float_t s);

	vector4 *vector4_normalize(vector4 *out, const vector4 *in);

	vector4 *vector4_random_dir_hemisphere(vector4 *out, const vector4 *dir, uint16_t *rseed);
	
	vector4 *vector4_random_dir_sphere(vector4 *out, uint16_t *rseed);

#ifdef __cplusplus
}
#endif