#include "matrix4.h"
#include "debug/debug.h"

const matrix4 MATRIX4_IDENTITY = { 1.0f, .0f, .0f, .0f, 
									.0f, 1.0f, .0f, .0f,
									.0f, .0f, 1.0f, .0f,
									.0f, .0f, .0f, 1.0f};

matrix4 *matrix4_load_rotation(matrix4 *out, const vector4 *rad)
{
	const float_t sx = sinf(rad->x), cx = cosf(rad->x);
	const float_t sy = sinf(rad->y), cy = cosf(rad->y);
	const float_t sz = sinf(rad->z), cz = cosf(rad->z);
	
	const float_t sycz = (sy * cz), sysz = (sy * sz);
	
	out->m00 = cy * cz;
	out->m01 = sycz * sx - cx * sz;
	out->m02 = sycz * cx + sx * sz;
	out->m10 = cy * sz;
	out->m11 = sysz * sx + cx * cz;
	out->m12 = sysz * cx - sx * cz;
	out->m20 = -sy;
	out->m21 = cy * sx;
	out->m22 = cy * cx;

	return out;
}

matrix4 *matrix4_load_translation(matrix4 *out, const vector4 *translation)
{
	out->m03 = translation->x;
	out->m13 = translation->y;
	out->m23 = translation->z;
	out->m33 = 1.0f;

	return out;
}

matrix4 *matrix4_transpose(matrix4 *out, const matrix4 *lhs)
{
	ASSERT(out != lhs);

	out->m16[0] = lhs->m16[0];
	out->m16[1] = lhs->m16[4];
	out->m16[2] = lhs->m16[8];
	out->m16[3] = lhs->m16[12];

	out->m16[4] = lhs->m16[1];
	out->m16[5] = lhs->m16[5];
	out->m16[6] = lhs->m16[9];
	out->m16[7] = lhs->m16[13];

	out->m16[8] = lhs->m16[2];
	out->m16[9] = lhs->m16[6];
	out->m16[10] = lhs->m16[10];
	out->m16[11] = lhs->m16[14];

	out->m16[12] = lhs->m16[3];
	out->m16[13] = lhs->m16[7];
	out->m16[14] = lhs->m16[11];
	out->m16[15] = lhs->m16[15];

	return out;
}

/*
 * Using Cramer's rule, quite unstable numerically
 */
matrix4 *matrix4_inverse(matrix4 *out, const matrix4 *lhs)
{
	ASSERT(out != lhs);

	float_t tmp[12];

	tmp[0]  = lhs->m22 * lhs->m33;
	tmp[1]  = lhs->m32 * lhs->m23;
	tmp[2]  = lhs->m12 * lhs->m33;
	tmp[3]  = lhs->m32 * lhs->m13;
	tmp[4]  = lhs->m12 * lhs->m23;
	tmp[5]  = lhs->m22 * lhs->m13;
	tmp[6]  = lhs->m02 * lhs->m33;
	tmp[7]  = lhs->m32 * lhs->m03;
	tmp[8]  = lhs->m02 * lhs->m23;
	tmp[9]  = lhs->m22 * lhs->m03;
	tmp[10] = lhs->m02 * lhs->m13;
	tmp[11] = lhs->m12 * lhs->m03;

	out->m00  = tmp[0] * lhs->m11 + tmp[3] * lhs->m21 + tmp[4]  * lhs->m31;
	out->m00 -= tmp[1] * lhs->m11 + tmp[2] * lhs->m21 + tmp[5]  * lhs->m31;
	out->m01  = tmp[1] * lhs->m01 + tmp[6] * lhs->m21 + tmp[9]  * lhs->m31;
	out->m01 -= tmp[0] * lhs->m01 + tmp[7] * lhs->m21 + tmp[8]  * lhs->m31;
	out->m02  = tmp[2] * lhs->m01 + tmp[7] * lhs->m11 + tmp[10] * lhs->m31;
	out->m02 -= tmp[3] * lhs->m01 + tmp[6] * lhs->m11 + tmp[11] * lhs->m31;
	out->m03  = tmp[5] * lhs->m01 + tmp[8] * lhs->m11 + tmp[11] * lhs->m21;
	out->m03 -= tmp[4] * lhs->m01 + tmp[9] * lhs->m11 + tmp[10] * lhs->m21;
	out->m10  = tmp[1] * lhs->m10 + tmp[2] * lhs->m20 + tmp[5]  * lhs->m30;
	out->m10 -= tmp[0] * lhs->m10 + tmp[3] * lhs->m20 + tmp[4]  * lhs->m30;
	out->m11  = tmp[0] * lhs->m00 + tmp[7] * lhs->m20 + tmp[8]  * lhs->m30;
	out->m11 -= tmp[1] * lhs->m00 + tmp[6] * lhs->m20 + tmp[9]  * lhs->m30;
	out->m12  = tmp[3] * lhs->m00 + tmp[6] * lhs->m10 + tmp[11] * lhs->m30;
	out->m12 -= tmp[2] * lhs->m00 + tmp[7] * lhs->m10 + tmp[10] * lhs->m30;
	out->m13  = tmp[4] * lhs->m00 + tmp[9] * lhs->m10 + tmp[10] * lhs->m20;
	out->m13 -= tmp[5] * lhs->m00 + tmp[8] * lhs->m10 + tmp[11] * lhs->m20;

	tmp[0]  = lhs->m20 * lhs->m31;
	tmp[1]  = lhs->m30 * lhs->m21;
	tmp[2]  = lhs->m10 * lhs->m31;
	tmp[3]  = lhs->m30 * lhs->m11;
	tmp[4]  = lhs->m10 * lhs->m21;
	tmp[5]  = lhs->m20 * lhs->m11;
	tmp[6]  = lhs->m00 * lhs->m31;
	tmp[7]  = lhs->m30 * lhs->m01;
	tmp[8]  = lhs->m00 * lhs->m21;
	tmp[9]  = lhs->m20 * lhs->m01;
	tmp[10] = lhs->m00 * lhs->m11;
	tmp[11] = lhs->m10 * lhs->m01;

	out->m20  = tmp[0]  * lhs->m13 + tmp[3]  * lhs->m23 + tmp[4]  * lhs->m33;
	out->m20 -= tmp[1]  * lhs->m13 + tmp[2]  * lhs->m23 + tmp[5]  * lhs->m33;
	out->m21  = tmp[1]  * lhs->m03 + tmp[6]  * lhs->m23 + tmp[9]  * lhs->m33;
	out->m21 -= tmp[0]  * lhs->m03 + tmp[7]  * lhs->m23 + tmp[8]  * lhs->m33;
	out->m22  = tmp[2]  * lhs->m03 + tmp[7]  * lhs->m13 + tmp[10] * lhs->m33;
	out->m22 -= tmp[3]  * lhs->m03 + tmp[6]  * lhs->m13 + tmp[11] * lhs->m33;
	out->m23  = tmp[5]  * lhs->m03 + tmp[8]  * lhs->m13 + tmp[11] * lhs->m23;
	out->m23 -= tmp[4]  * lhs->m03 + tmp[9]  * lhs->m13 + tmp[10] * lhs->m23;
	out->m30  = tmp[2]  * lhs->m22 + tmp[5]  * lhs->m32 + tmp[1]  * lhs->m12;
	out->m30 -= tmp[4]  * lhs->m32 + tmp[0]  * lhs->m12 + tmp[3]  * lhs->m22;
	out->m31  = tmp[8]  * lhs->m32 + tmp[0]  * lhs->m02 + tmp[7]  * lhs->m22;
	out->m31 -= tmp[6]  * lhs->m22 + tmp[9]  * lhs->m32 + tmp[1]  * lhs->m02;
	out->m32  = tmp[6]  * lhs->m12 + tmp[11] * lhs->m32 + tmp[3]  * lhs->m02;
	out->m32 -= tmp[10] * lhs->m32 + tmp[2]  * lhs->m02 + tmp[7]  * lhs->m12;
	out->m33  = tmp[10] * lhs->m22 + tmp[4]  * lhs->m02 + tmp[9]  * lhs->m12;
	out->m33 -= tmp[8]  * lhs->m12 + tmp[11] * lhs->m22 + tmp[5]  * lhs->m02;

	const float_t det = (lhs->m00 * out->m00 + lhs->m10 * out->m01 + lhs->m20 * out->m02 + lhs->m30 * out->m03);

	const float_t idet = 1.0f / det;
	out->m00 *= idet;
	out->m01 *= idet;
	out->m02 *= idet;
	out->m03 *= idet;
	out->m10 *= idet;
	out->m11 *= idet;
	out->m12 *= idet;
	out->m13 *= idet;
	out->m20 *= idet;
	out->m21 *= idet;
	out->m22 *= idet;
	out->m23 *= idet;
	out->m30 *= idet;
	out->m31 *= idet;
	out->m32 *= idet;
	out->m33 *= idet;
	
	return out;
}

matrix4 *matrix4_multiply(matrix4 *out, const matrix4 *lhs, const matrix4 *rhs)
{
	ASSERT(out != lhs && out != rhs);

	out->m00 = lhs->m00 * rhs->m00 + lhs->m01 * rhs->m10 + lhs->m02 * rhs->m20 + lhs->m03 * rhs->m30;
	out->m10 = lhs->m10 * rhs->m00 + lhs->m11 * rhs->m10 + lhs->m12 * rhs->m20 + lhs->m13 * rhs->m30;
	out->m20 = lhs->m20 * rhs->m00 + lhs->m21 * rhs->m10 + lhs->m22 * rhs->m20 + lhs->m23 * rhs->m30;
	out->m30 = lhs->m30 * rhs->m00 + lhs->m31 * rhs->m10 + lhs->m32 * rhs->m20 + lhs->m33 * rhs->m30;
	out->m01 = lhs->m00 * rhs->m01 + lhs->m01 * rhs->m11 + lhs->m02 * rhs->m21 + lhs->m03 * rhs->m31;
	out->m11 = lhs->m10 * rhs->m01 + lhs->m11 * rhs->m11 + lhs->m12 * rhs->m21 + lhs->m13 * rhs->m31;
	out->m21 = lhs->m20 * rhs->m01 + lhs->m21 * rhs->m11 + lhs->m22 * rhs->m21 + lhs->m23 * rhs->m31;
	out->m31 = lhs->m30 * rhs->m01 + lhs->m31 * rhs->m11 + lhs->m32 * rhs->m21 + lhs->m33 * rhs->m31;
	out->m02 = lhs->m00 * rhs->m02 + lhs->m01 * rhs->m12 + lhs->m02 * rhs->m22 + lhs->m03 * rhs->m32;
	out->m12 = lhs->m10 * rhs->m02 + lhs->m11 * rhs->m12 + lhs->m12 * rhs->m22 + lhs->m13 * rhs->m32;
	out->m22 = lhs->m20 * rhs->m02 + lhs->m21 * rhs->m12 + lhs->m22 * rhs->m22 + lhs->m23 * rhs->m32;
	out->m32 = lhs->m30 * rhs->m02 + lhs->m31 * rhs->m12 + lhs->m32 * rhs->m22 + lhs->m33 * rhs->m32;
	out->m03 = lhs->m00 * rhs->m03 + lhs->m01 * rhs->m13 + lhs->m02 * rhs->m23 + lhs->m03 * rhs->m33;
	out->m13 = lhs->m10 * rhs->m03 + lhs->m11 * rhs->m13 + lhs->m12 * rhs->m23 + lhs->m13 * rhs->m33;
	out->m23 = lhs->m20 * rhs->m03 + lhs->m21 * rhs->m13 + lhs->m22 * rhs->m23 + lhs->m23 * rhs->m33;
	out->m33 = lhs->m30 * rhs->m03 + lhs->m31 * rhs->m13 + lhs->m32 * rhs->m23 + lhs->m33 * rhs->m33;

	return out;
}

vector4 *matrix4_transform_vector4(vector4 *out, const matrix4 *lhs, const vector4 *rhs)
{
	ASSERT(out != rhs);

	out->x = rhs->x * lhs->m00 + rhs->y * lhs->m01 + rhs->z * lhs->m02 + rhs->w * lhs->m03;
	out->y = rhs->x * lhs->m10 + rhs->y * lhs->m11 + rhs->z * lhs->m12 + rhs->w * lhs->m13;
	out->z = rhs->x * lhs->m20 + rhs->y * lhs->m21 + rhs->z * lhs->m22 + rhs->w * lhs->m23;
	out->w = rhs->x * lhs->m30 + rhs->y * lhs->m31 + rhs->z * lhs->m32 + rhs->w * lhs->m33;

	return out;
}
