#include "math/color.h"

color8 color32_to_color8(const color32 *in)
{
	const color8 res = {
		(uint8_t)(255 * in->b),
		(uint8_t)(255 * in->g),
		(uint8_t)(255 * in->r),
		(uint8_t)(255 * in->a)
	};

	return res;
}

color32 *color32_scale(color32 *out, const color32 *in, float_t s)
{
	out->r = in->r * s;
	out->g = in->g * s;
	out->b = in->b * s;

	return out;
}

color32 *color32_pow(color32 *out, const color32 *in, float_t p)
{
	out->r = fast_powf(in->r, p);
	out->g = fast_powf(in->g, p);
	out->b = fast_powf(in->b, p);

	return out;
}

color32 *color32_add(color32 *out, const color32 *lhs, const color32 *rhs)
{
	out->r = lhs->r + rhs->r;
	out->g = lhs->g + rhs->g;
	out->b = lhs->b + rhs->b;

	return out;
}

color32 *color32_multiply(color32 *out, const color32 *lhs, const color32 *rhs)
{
	out->r = lhs->r * rhs->r;
	out->g = lhs->g * rhs->g;
	out->b = lhs->b * rhs->b;

	return out;
}

color32 *color32_clamp(color32 *out, const color32 *in)
{
	out->r = (in->r > 1.f) ? 1.f : (in->r < .0f) ? .0f : in->r;
	out->g = (in->g > 1.f) ? 1.f : (in->g < .0f) ? .0f : in->g;
	out->b = (in->b > 1.f) ? 1.f : (in->b < .0f) ? .0f : in->b;

	return out;
}
