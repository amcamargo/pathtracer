#include "fastmath.h"

float_t fast_powf(float_t a, float_t b);

float_t fast_sqrtf(float_t in);

float_t fast_invsqrtf(float_t in);

float_t fast_absf(float_t in);

void swap(float_t *a, float_t *b);

uint8_t quadratic(float_t a, float_t b, float_t c, float_t *root0, float_t *root1);

void _dorand48(unsigned short xseed[3])
{
	const uint16_t RAND48_ADD = 0x000b;
	const uint16_t _rand48_mult[3] = { 0xe66d, 0xdeec, 0x0005 };

	uint32_t accu;
	uint16_t temp[2];

	accu = (uint32_t)_rand48_mult[0] * (uint32_t)xseed[0] + (uint32_t)RAND48_ADD;
	temp[0] = (uint16_t)accu; /* lower 16 bits */
	accu >>= sizeof(uint16_t) * 8;
	accu += (uint32_t)_rand48_mult[0] * (uint32_t)xseed[1] + (uint32_t)_rand48_mult[1] * (uint32_t)xseed[0];
	temp[1] = (uint16_t)accu; /* middle 16 bits */
	accu >>= sizeof(uint16_t) * 8;
	accu += _rand48_mult[0] * xseed[2] + _rand48_mult[1] * xseed[1] + _rand48_mult[2] * xseed[0];
	xseed[0] = temp[0];
	xseed[1] = temp[1];
	xseed[2] = (uint16_t)accu;
}

/*
 * example seed:
 * uint16_t _rand48_seed[3] = { 0x330e, 0xabcd, 0x1234 };
 */
float_t frand48(uint16_t xseed[3])
{
	_dorand48(xseed);
	return ldexpf((float_t)xseed[0], -48) + 
		   ldexpf((float_t)xseed[1], -32) + 
		   ldexpf((float_t)xseed[2], -16);
}
