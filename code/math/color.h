#pragma once

#include "fastmath.h"

typedef struct color8
{
	union {
		uint8_t values[4];
		struct {
			uint8_t r;
			uint8_t g;
			uint8_t b;
			uint8_t a;
		};
	};
} color8;

typedef struct color32
{
	union {
		float_t values[4];
		struct {
			float_t r;
			float_t g;
			float_t b;
			float_t a;
		};
	};
} color32;

#ifdef __cplusplus
extern "C" {
#endif

	/*
	 * Converts from float to 32bit color, and moves to BGRA
	 */
	color8 color32_to_color8(const color32 *in);

	color32 *color32_scale(color32 *out, const color32 *in, float_t s);

	color32 *color32_pow(color32 *out, const color32 *in, float_t p);

	color32 *color32_add(color32 *out, const color32 *lhs, const color32 *rhs);

	color32 *color32_multiply(color32 *out, const color32 *lhs, const color32 *rhs);

	color32 *color32_clamp(color32 *out, const color32 *in);

#ifdef __cplusplus
}
#endif