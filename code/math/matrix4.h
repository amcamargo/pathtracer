#pragma once

#include "vector4.h"

typedef struct matrix4
{
	union {
		float_t m16[16];
		
		struct {
			float_t m00, m01, m02, m03;
			float_t m10, m11, m12, m13;
			float_t m20, m21, m22, m23;
			float_t m30, m31, m32, m33;
		};
	};
} matrix4;

#ifdef __cplusplus
extern "C" {
#endif

	extern const matrix4 MATRIX4_IDENTITY;

	matrix4 *matrix4_load_rotation(matrix4 *out, const vector4 *rad);

	matrix4 *matrix4_load_translation(matrix4 *out, const vector4 *translation);

	matrix4 *matrix4_transpose(matrix4 *out, const matrix4 *lhs);
		
	matrix4 *matrix4_inverse(matrix4 *out, const matrix4 *lhs);

	matrix4 *matrix4_multiply(matrix4 *out, const matrix4 *lhs, const matrix4 *rhs);

	vector4 *matrix4_transform_vector4(vector4 *out, const matrix4 *lhs, const vector4 *rhs);

#ifdef __cplusplus
}
#endif