#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include <inttypes.h>

#include <xmmintrin.h>
#include <emmintrin.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef union __m128f
{
	__m128 mm;
	float_t v[4];
} __m128f;

/*
 * TODO
 */
inline float_t fast_powf(float_t a, float_t b)
{
	//// calculate approximation with fraction of the exponent
	//int32_t e = (int32_t)b;
	//union {
	//	double_t d;
	//	int32_t x[2];
	//} u = { a };
	//u.x[1] = (int32_t)((b - e) * (u.x[1] - 1072632447) + 1072632447);
	//u.x[0] = 0;

	//// exponentiation by squaring with the exponent's integer part
	//// double r = u.d makes everything much slower, not sure why
	//double_t r = 1.0;
	//while (e) {
	//	if (e & 1) {
	//		r *= a;
	//	}
	//	a *= a;
	//	e >>= 1;
	//}

	//return (float_t)(r * u.d);

	return powf(a, b);
}

inline float_t fast_sqrtf(float_t in)
{
	__m128f op;
	op.mm = _mm_sqrt_ss(_mm_set_ss(in));
	return op.v[0];
}

inline float_t fast_invsqrtf(float_t in)
{
	//__m128 xmm3 = _mm_set_ss(in);
	//__m128 xmm2 = _mm_set_ss(3.0f);
	//__m128 xmm1 = _mm_set_ss(0.5f);
	//
	//// first "guess"
	//const __m128 xmm0 = _mm_rsqrt_ss(xmm3); 
	//
	//// refine result
	//xmm3 = _mm_mul_ss(xmm0, xmm3);
	//xmm1 = _mm_mul_ss(xmm0, xmm1);
	//xmm3 = _mm_mul_ss(xmm0, xmm3);
	//xmm2 = _mm_sub_ss(xmm2, xmm3);
	//xmm1 = _mm_mul_ss(xmm2, xmm1);

	//__m128f out;
	//out.mm = xmm1;
	//
	//return out.v[0];

	__m128f op;
	op.mm = _mm_div_ss(_mm_set_ss(1.f), _mm_sqrt_ss(_mm_set_ss(in)));
	return op.v[0];
}

inline float_t fast_absf(float_t in)
{
	__m128f op;
	op.mm = _mm_andnot_ps(_mm_castsi128_ps(_mm_set1_epi32(0x80000000)), _mm_set_ss(in));
	return op.v[0];
}

inline void swap(float_t *a, float_t *b)
{
	const float_t tmp = *a;
	*a = *b;
	*b = tmp;
}

inline uint8_t quadratic(float_t a, float_t b, float_t c, float_t *root0, float_t *root1)
{
	const float_t delta = (b * b) - (4.0f * a * c);
	
	if (delta < .0f) 
		return 0;
	
	const float_t rootDelta = fast_sqrtf(delta);

	const float q = (b < .0f) ? -.5f * (b - rootDelta) 
							  : -.5f * (b + rootDelta);
	
	*root0 = q / a;
	*root1 = c / q;

	if (*root0 > *root1)
		swap(root0, root1);

	return 1;
}

float_t frand48(uint16_t xseed[3]);

#ifdef __cplusplus
}
#endif
