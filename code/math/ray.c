#include "ray.h"

vector4 *ray_get_position(vector4 *out, const ray *r, float_t t)
{
	vector4 tmp;
	*out = r->origin;
	vector4_add(out, out, vector4_scale(&tmp, &r->direction, t));
	return out;
}