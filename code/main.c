#if defined(_WIN32) // fix SDL's incomplete windows includes
#define WIN32_LEAN_AND_MEAN
#define EXTRA_LEAN
#include <Windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#define HAVE_M_PI
#include <SDL.h>
#undef main // another wtf from SDL

#include "debug/debug.h"
#include "system/threadpool.h"

#include "render/surface.h"
#include "render/camera.h"
#include "math/sphere.h"
#include "math/color.h"

#define SCREEN_W 640
#define SCREEN_H 480

typedef enum reflectance
{
	DIFFUSE,
	SPECULAR,
	REFRACTIVE
} reflectance;

typedef struct material {
	color32 color;
	color32 emission;
	reflectance refl;
} material;

typedef struct worker_job
{
	size_t thread_id;
	size_t start;
	size_t end;
	surface *colors;
	surface *pixels;
	camera *cam;
	SDL_atomic_t s;
} worker_job;

const vector4 params[] = {
	{ .0f, .0f,-20.0f, 1.0f },
	{ .0f, .0f,   .0f, 1.0f },
	{ .0f, 1.0f,  .0f,  .0f },
};

const sphere scene[] = {
	{ { -1.0f,   .0f,   .0f,  .0f }, 10.0f, PLANE },
	{ {  1.0f,   .0f,   .0f,  .0f }, 10.0f, PLANE },
	{ {   .0f, -1.0f,   .0f,  .0f }, 10.0f, PLANE },
	{ {   .0f,  1.0f,   .0f,  .0f }, 10.0f, PLANE },
	{ {   .0f,   .0f, -1.0f,  .0f }, 20.0f, PLANE },
	{ {   .0f,   .0f,  1.0f,  .0f }, 20.0f, PLANE },
	{ { -2.0f, -5.0f, 10.0f, 1.0f },  2.0f, SPHERE },
	{ {  5.0f,  4.0f, 15.0f, 1.0f },  4.f, SPHERE },
	{ {   .0f,  0.0f, 4.0f, 1.0f },   1.5f, SPHERE }, //light
};

const material materials[] = {
	{ { .75f, .25f, .25f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .25f, .25f, .75f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .75f, .75f, .75f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .75f, .75f, .75f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .75f, .75f, .75f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .75f, .75f, .75f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ { .25f, .75f, .25f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, DIFFUSE },
	{ {  1.f,  1.f,  1.f, 1.f }, {  .0f,  .0f,  .0f,  .0f }, SPECULAR },
	{ { 1.0f, 1.0f, 1.0f, .0f }, { 20.f, 20.f, 20.f, 1.0f }, DIFFUSE }, //light
};

const size_t scene_size = sizeof(scene) / sizeof(scene[0]);

float_t scene_intersect_ray(uint32_t *id, const ray *lhs)
{
	float_t t = FLT_MAX;
	for (uint32_t s = 0; s < scene_size; ++s) {
		const sphere *sph = &scene[s];
		const float_t res = (sph->type == PLANE) ? plane_intersect_ray(sph, lhs)
												 : sphere_intersect_ray(sph, lhs);
		if (res != .0f && res < t) {
			t = res;
			*id = s;
		}
	}

	return t;
}

color32 *radiance(color32 *out, const ray *r, uint32_t depth, uint16_t *rseed)
{
	uint32_t id;
	const float_t t = scene_intersect_ray(&id, r);

	if (t == FLT_MAX)
		return out;

	const sphere *object = &scene[id];
	const material *mat = &materials[id];

	color32 mat_color = mat->color;

	if (depth > 5)
	{
		float_t p = mat_color.r > mat_color.g && mat_color.r > mat_color.b ? 
															   mat_color.r : mat_color.g > mat_color.b ? 
															   mat_color.g : mat_color.b;
		if (frand48(rseed) < p)
			color32_scale(&mat_color, &mat_color, 1.f / p);
		else
		{
			color32_add(out, out, &mat->emission);
			return out;
		}
	}
	
	vector4 hit;
	ray_get_position(&hit, r, t);
	vector4 normal;
	sphere_get_normal(&normal, object, &hit);

	color32 res = { .0f, .0f, .0f, 1.0f };

	switch (mat->refl)
	{
	case DIFFUSE:
	default:
		{
			ray refl_ray = {
				hit, VECTOR4_ZERO
			};

			vector4_random_dir_hemisphere(&refl_ray.direction, &normal, rseed);
			
			color32_multiply(&res, &mat_color, radiance(&res, &refl_ray, ++depth, rseed));
			color32_add(&res, &res, &mat->emission);
		}
		break;
	case SPECULAR:
		{
			ray refl_ray = {
				hit, VECTOR4_ZERO
			};

			vector4_add(&refl_ray.direction, &r->direction, vector4_scale(&refl_ray.direction, &normal, -1.f));
			vector4_scale(&refl_ray.direction, &refl_ray.direction, vector4_dot(&normal, &r->direction) * 2.f);

			color32_multiply(&res, &mat_color, radiance(&res, &refl_ray, ++depth, rseed));
			color32_add(&res, &res, &mat->emission);
		}

		break;
	case REFRACTIVE:
		break;
	}

	color32_add(out, out, &res);

	return out;
}

SDL_atomic_t KeepGoing;

void pathtracer_loop(void *data)
{
	worker_job *job = (worker_job*)data;

	const float_t gamma = 1.f / 2.2f;
	const uint32_t total = job->colors->width * job->colors->height;
	
	color32 *colors = (color32*)job->colors->buffer;
	color8 *pixels = (color8*)job->pixels->buffer;
	uint16_t rseed[3] = { 0x0, 0x0, job->colors->height * (uint32_t)job->thread_id };

	const vector4 weights[4] = {
		{ .25f, .25f },
		{ .25f, .75f, },
		{ .75f, .25f, },
		{ .75f, .75f, },
	};
	const size_t weights_sz = sizeof(weights) / sizeof(weights[0]);

	size_t s = 1;
	while (SDL_AtomicGet(&KeepGoing))
	{
		for (size_t n = job->start; n < job->end; ++n) {
			// flip horizontally
			const float_t j = (float_t)(total - n) / (float_t)job->colors->width;
			// proceed normally
			const float_t i = (float_t)(n % job->colors->width);

			color32 res = { 0, 0, 0, 1 };
			ray r;
			camera_get_ray(&r, job->cam, i + weights[s%weights_sz].x, j + weights[s%weights_sz].y);
			radiance(&res, &r, 0, rseed);
			
			color32_add(&colors[n], &colors[n], &res);
			color32_scale(&res, &colors[n], 1.f / (float_t)s);
			color32_pow(&res, color32_clamp(&res, &res), gamma);
			pixels[n] = color32_to_color8(&res);
		}
		SDL_AtomicAdd(&job->s, 1);
		++s;
	}
}

int main(int argc, char *argv[])
{
	SDL_Window* window;
	SDL_Surface* screen;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return EXIT_FAILURE;
	}

	window = SDL_CreateWindow("pathtracer",
							  SDL_WINDOWPOS_CENTERED,
							  SDL_WINDOWPOS_CENTERED,
							  SCREEN_W,
							  SCREEN_H,
							  SDL_WINDOW_SHOWN);

	if (window == NULL) {
		return EXIT_FAILURE;
	}

	SDL_ShowWindow(window);

	screen = SDL_GetWindowSurface(window);

	surface image;
	surface_create(&image, SCREEN_W, SCREEN_H, sizeof(color32));
	surface image_c8;
	surface_create(&image_c8, SCREEN_W, SCREEN_H, sizeof(color8));
	camera cam;
	camera_create(&cam, &params[0], &params[1], &params[2], (float_t)image.width, (float_t)image.height, .78f);

	const size_t num_threads = threadpool_hardware_concurrency() - 1;
	threadpool_t *tp = threadpool_create((int)num_threads, NULL, NULL);
	worker_job *work = malloc(sizeof(worker_job) * num_threads);
	SDL_AtomicSet(&KeepGoing, 1);

	const size_t pixels = image.width * image.height;
	const size_t stride = pixels / num_threads;
	for (size_t i = 0, p = 0; i < num_threads; ++i, p += stride)
	{
		work[i].thread_id = i;
		work[i].start = p;
		work[i].end = (i != num_threads - 1) ? p + stride : pixels;
		work[i].colors = &image;
		work[i].pixels = &image_c8;
		work[i].cam = &cam;
		SDL_AtomicSet(&work[i].s, 0);

		threadpool_schedule(tp, pathtracer_loop, &work[i]);
	}

	SDL_Event e;
	uint32_t quit = 0;
	uint32_t last = 0;
	while (!quit) {
		while (SDL_PollEvent(&e) != 0) {
			switch (e.type) {
				case SDL_QUIT:
					quit = 1;
					break;
				case SDL_KEYDOWN:
					switch (e.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							quit = 1;
							break;
						default:
							break;
					}
					break;
				default: 
					break;
			}
		}

		if ((SDL_GetTicks() - last) > 1000)
		{
			last = SDL_GetTicks();

			SDL_Surface *sdl_surface = SDL_CreateRGBSurfaceFrom(image_c8.buffer, SCREEN_W, SCREEN_H, 32, SCREEN_W * 4, 0, 0, 0, 0);
			SDL_BlitSurface(sdl_surface, NULL, screen, NULL);
			SDL_UpdateWindowSurface(window);
			SDL_FreeSurface(sdl_surface);

			size_t samples = 0;
			for (size_t i = 0; i < num_threads; ++i)
			{
				samples += SDL_AtomicGet(&work[i].s);
			}

			samples /= num_threads;

			char title[128];

			snprintf(title, sizeof(title), "pathtracer - %llu samples", samples);

			SDL_SetWindowTitle(window, title);
		}
	}

	SDL_AtomicSet(&KeepGoing, 0);

	threadpool_die(tp, 1);
	threadpool_destroy(tp);
	
	surface_free(&image);
	surface_free(&image_c8);
	free(work);
	
	SDL_DestroyWindow(window);
	SDL_Quit();
	
	return 0;
}
