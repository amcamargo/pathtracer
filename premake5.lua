Dependency = require("dependency")

local sdl2 = Dependency:new("SDL2", "https://www.libsdl.org/release/SDL2-devel-2.0.9-VC.zip", "SDL2-2.0.9/include", "SDL2-2.0.9/lib/x64")
-- local imgui = Dependency:new("imgui", "https://github.com/ocornut/imgui/archive/master.zip", "", "")

wks = workspace "Pathtracer"
	location "build"
	language "C"
	-- platforms { "Win64" }
	architecture "x86_64"
	cdialect "gnu11"
	vectorextensions "SSE2"
	configurations { "Debug", "Release" }

	targetdir "%{wks.location}/%{cfg.shortname}/%{prj.name}"
	objdir "%{wks.location}/%{cfg.shortname}/%{prj.name}/obj"

	filter { "configurations:Debug" }
		defines { "_DEBUG" }
		symbols "On"
		optimize "Off"

	filter { "configurations:Release" }
		defines { "_NDEBUG" }
		symbols "Off"
		-- optimize "Full"
		optimize "Speed"

	filter {}
	
	-- filter { "system:windows" }
		-- systemversion "10.0.16299.0" -- WinSDK ver
		
	filter {}

project "pathtracer"
	kind "WindowedApp"
	defines {}
	includedirs { "code" }

	files {
		"code/**.c",
		"code/**.h"
	}
	
	if os.istarget("windows") then
		sdl2:setup("deps")
		includedirs { sdl2.include_dir }
		libdirs { sdl2.lib_dir }
		
		local dll = string.gsub("%{wks.location}../" .. sdl2.lib_dir .. "/SDL2.dll", '/', '\\')
		postbuildcommands  { 'copy /Y "' .. dll .. '" "%{wks.location}%{cfg.targetdir}"' }
	end
	
	filter "system:windows"
		buildoptions { "/MP" }
		links { "SDL2" }
		linkoptions { "/ENTRY:mainCRTStartup" }
	filter "system:linux"
		links { "SDL2", "m" }
	filter {}
