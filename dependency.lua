-- dependency.lua
-- Compatible with Premake 5.x

local Dependency = 
{
	name = "",
	artifact_url = "",
	include_dir = "",
	lib_dir = "",
}

Dependency.__index = Dependency

function Dependency:new(name, url, include, lib)
	local out = {}
	setmetatable(out, self)
	out.name = name
	out.artifact_url = url
	out.include_dir = include
	out.lib_dir = lib
	return out
end

function fn_progress(total, current)
	local anim = { '-', '\\', '|', '/' }
	local idx = math.floor(math.fmod((current / total) * 100, 4)) + 1;
	local mega = 1048576;
	io.write(string.format("Progress (%s, %.2fMB, %.2fMB)\r", anim[idx], current/mega, total/mega))
end

function Dependency:setup(path)
	if not os.isdir(path) then
		os.mkdir(path)
	end
	
	print("> Configuring " .. self.name)
	
	local idx = string.findlast(self.artifact_url, '/', true)
	local fname = string.sub(self.artifact_url, idx+1, string.len(self.artifact_url))
	fname = path .. '/' .. fname;
	
	local fextract = path .. '/' .. self.name
	local dep_check = fextract .. "/dep.check"
	
	local result_str = "OK"
	local response_code = 0
	
	if not os.isfile(dep_check) then -- check file
		print("> Dowloading " .. self.name)
		result_str, response_code = http.download(self.artifact_url, fname, fn_progress)
		io.write("\r\n")
	end
	
	if result_str == "OK" then
		if not os.isfile(dep_check) then
			print("> Extracting " .. self.name)
			os.mkdir(fextract)
			zip.extract(fname, fextract)
			
			os.touchfile(dep_check) -- create check file
			if not os.isfile(dep_check) then
				premake.error("Extraction failed for file " .. fname)
			end
		end
		
		self.include_dir = fextract .. '/' .. self.include_dir
		self.lib_dir = fextract .. '/' .. self.lib_dir
	else
		premake.error(string.format("Download failed (%d): %s", response_code, result_str))
	end
end

return Dependency